# NIfTI to DICOM

This gear converts NIfTI images, and associated JSON sidecar into DICOMs.

The metadata used to populate the required fields in the dicom comes from first the
sidecar, second the Flywheel hierarchy, and lastly, is generated if still missing

## Usage

A single NIfTI will be provided as input.  The gear will find the corresponding sidecar
in the same acquisition.

### Inputs

* `nifti-file`: A NIfTI1 file.

### Configuration

* __debug__ (boolean, default False): Include debug statements in output.

### Limitations

Current limitations of the gear are as follows:

* Only NIfTI1 (not NIfTI2) is supported.
* Only Enhanced Multiframe DICOMs will be output
* Only MR and CT modalities will be accepted
* Z-axis direction is not guarenteed to be the same is the original DICOM from which the
  NIfTI was generated

## Contributing

For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
