"""General utilities module."""
import gzip
import logging

log = logging.getLogger(__name__)


def is_gzip(file_name) -> bool:
    """Sniff file to see if it's a gzip."""
    is_gzip = False
    file_ = None
    try:
        file_ = gzip.open(file_name, "rb")
        file_.peek(0)
        is_gzip = True
    except gzip.BadGzipFile:
        log.info("File is not gzipped, opening normally")
    finally:
        if not getattr(file_, "closed", True):
            file_.close()  # type: ignore
    return is_gzip
