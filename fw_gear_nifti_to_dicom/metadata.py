"""DICOM metadata module."""
import json
import logging
import os
import typing as t
from pathlib import Path

import backoff
from fw_core_client import ClientError, CoreClient, ServerError
from fw_file.dicom import generate_uid

log = logging.getLogger(__name__)

NEEDED_FIELDS = (
    "PatientName",  # subject.firstname + subject.lastname
    "PatientID",  # subject.label
    "StudyID",  # session.label
    "SeriesNumber",
    "InstanceNumber",
    "SOPInstanceUID",
    "SeriesInstanceUID",  # acquisition.uid
    "StudyInstanceUID",  # session.uid
    "Modality",  # file.modality
    "SOPClassUID",  # file.modality
)


def get_metadata(fw: CoreClient, file_name: Path, parent_ref: t.Dict) -> t.Dict:
    """Get metadata for creating dicom.

    First try fields from sidecar, if not present, pull from hierarchy,
    or generate if necessary.
    """
    if parent_ref["type"] != "acquisition":
        raise ValueError(
            "This gear only supports acquisition files. Please run at acquisition level"
        )
    sidecar, file_ = get_sidecar(fw, str(file_name), parent_ref)
    metadata = {k: sidecar.get(k) for k in NEEDED_FIELDS}
    if all(v is not None for v in metadata.values()):
        return metadata
    acq = fw.get(f"/{parent_ref['type']}s/{parent_ref['id']}")
    parents = acq.parents
    subject = fw.get(f"/subjects/{parents.subject}")
    session = fw.get(f"/sessions/{parents.session}")
    if not metadata["PatientName"]:
        firstname = subject.firstname
        lastname = subject.lastname
        name = None
        if lastname:
            name = lastname
        if firstname:
            name += "^" + firstname
        metadata["PatientName"] = name if name else "Anonymous"
    if not metadata["PatientID"]:
        metadata["PatientID"] = subject.label if subject.label else "Anonymous"
    if not metadata["StudyID"]:
        metadata["StudyID"] = session.label if session.label else "Anonymized"
    if not metadata["SeriesNumber"]:
        metadata["SeriesNumber"] = 0
    if not metadata["InstanceNumber"]:
        metadata["InstanceNumber"] = 0
    if not metadata["SeriesInstanceUID"]:
        uid = None
        if acq.uid:
            uid = acq.uid.split("_")[0]
        metadata["SeriesInstanceUID"] = uid if uid else generate_uid()
    if not metadata["StudyInstanceUID"]:
        metadata["StudyInstanceUID"] = session.uid if session.uid else generate_uid()
    if not metadata["SOPInstanceUID"]:
        metadata["SOPInstanceUID"] = generate_uid()
    if not metadata["Modality"]:
        if file_["modality"]:
            metadata["Modality"] = file_["modality"]
        else:
            metadata["Modality"] = "MR"
    if not metadata["SOPClassUID"]:
        if metadata["Modality"] == "MR":
            metadata["SOPClassUID"] = "1.2.840.10008.5.1.4.1.1.4.1"
        elif metadata["Modality"] == "CT":
            metadata["SOPClassUID"] = "1.2.840.10008.5.1.4.1.1.2.1"
        else:
            raise NotImplementedError(f"Unsupported modality {metadata['Modality']}")
    return metadata


def get_retry_time() -> int:
    """Helper function to return retry time from env."""
    return int(os.getenv("NIFTI_RETRY_TIME", "30"))


@backoff.on_exception(backoff.expo, (ClientError, ServerError), max_time=get_retry_time)
@backoff.on_exception(backoff.expo, ValueError, max_time=get_retry_time)
def get_sidecar(  # pylint: disable=too-many-locals
    fw: CoreClient, file_name: str, parent_ref: t.Dict
) -> t.Tuple[t.Dict, t.Dict]:
    """Get json sidecar."""
    log.info(f"Attempting to get sidecar for nifti {file_name}")
    candidates = fw.get(f"/{parent_ref['type']}s/{parent_ref['id']}").get("files", [])
    sidecar = None
    # Loop through files in the acquisition
    for file_ in candidates:
        # Get source code files
        if file_["type"] not in ["source code"]:
            continue
        try:
            # Get file root, continue if file doesn't end with `.json`
            name, _ = file_["name"].split(".json")
            # Split out root name, leaving only extension
            _, extension = file_name.split(name)
        except ValueError:
            continue
        # If extension is a nifti, this is our file.
        if extension in [".nii", ".nii.gz"]:
            sidecar = file_
            log.info(f"Found sidecar {file_.name}")
            break
    if sidecar is None:
        raise ValueError("Could not find NIfTI sidecar.")

    resp = fw.get(f"/files/{sidecar['file_id']}/download", stream=True)
    sidecar_json = json.loads(resp.content)
    return sidecar_json, sidecar
