"""Handle pixelmed function."""
import logging
import os
import subprocess
from pathlib import Path

log = logging.getLogger(__name__)


def run_pixelmed_conversion(
    file_: Path, file_name: str, out_dir: Path, metadata: dict
) -> Path:
    """Wrap pixelmed executable."""
    cmd = [
        "java",
        "-cp",
        os.getenv("PIXELMED"),
        "com.pixelmed.convert.NIfTI1ToDicom",
        str(file_),  # Input file name
        str(out_dir / file_name),  # Output file name
        str(metadata["PatientName"]),
        str(metadata["PatientID"]),
        str(metadata["StudyID"]),
        str(metadata["SeriesNumber"]),
        str(metadata["InstanceNumber"]),
        str(metadata["Modality"]),
        str(metadata["SOPClassUID"]),
    ]
    log.info(f"Running pixelmed command: {' '.join(cmd)}")
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while True:
        if process.poll() is None:
            out, _ = process.communicate()
            print(out.decode("utf-8").strip() + "\n")
        else:
            if process.returncode != 0:
                log.error(f"Pixelmed failed with exit_code: {process.returncode}")
                raise SystemExit(1)
            log.info("Pixelmed completed")
            return out_dir / file_name
