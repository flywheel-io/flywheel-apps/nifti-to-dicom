"""Main module."""
import gzip
import logging
import tempfile
from contextlib import ExitStack
from pathlib import Path

from fw_core_client import CoreClient

from .metadata import get_metadata
from .pixelmed import run_pixelmed_conversion
from .utils import is_gzip

log = logging.getLogger(__name__)


def run(nifti_file: dict, client: CoreClient, out_dir: Path):
    """Main entrypoint for nifti to dicom.

    Args:
        nifti_file (dict): config.json dictionary of file
        client (CoreClient): Client to connect to API
        out_dir (Path): Output directory
    """
    nifti_path = Path(nifti_file["location"]["path"])
    parent = nifti_file["hierarchy"]
    metadata = get_metadata(client, nifti_path, parent)
    root = nifti_path.name
    if ".gz" in root:
        root = root.split(".gz")[0]
    if ".nii" in root:
        root = root.split(".nii")[0]
    new_name = f"{root}.dcm"

    with ExitStack() as stack:
        # Conditionally enter gzip.open or open depending on whether or not the
        # file is gzipped
        fp = stack.enter_context(
            gzip.open(nifti_path, "rb")
            if is_gzip(nifti_path)
            else open(nifti_path, "rb")
        )
        # Get a temp file to write decompressed file to pass into pixelmed
        tmp_file = stack.enter_context(tempfile.NamedTemporaryFile())
        tmp_file.write(fp.read())
        file_ = Path(tmp_file.name)
        out_file = run_pixelmed_conversion(file_, new_name, out_dir, metadata)
        log.info(f"Saved output file {out_file}")
