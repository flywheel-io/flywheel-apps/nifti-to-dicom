"""Module to test parser.py"""
from unittest.mock import MagicMock

from fw_gear_nifti_to_dicom import __version__
from fw_gear_nifti_to_dicom.parser import parse_config


def test_parse_config():
    gc = MagicMock()
    gc.get_input.side_effect = ("nifti", {"key": "local.flywheel.io:test"})
    nift, fw = parse_config(gc)
    get_input_args = [call.args for call in gc.get_input.call_args_list]
    assert ("nifti-file",) in get_input_args
    assert ("api-key",) in get_input_args
    assert nift == "nifti"
    assert fw.config.api_key == "local.flywheel.io:test"
    assert fw.config.client_name == "fw_gear_nifti_to_dicom"
    assert fw.config.client_version == __version__
