from unittest.mock import MagicMock

import pytest
from fw_core_client import ClientError, CoreClient

from fw_gear_nifti_to_dicom.metadata import get_metadata, get_sidecar


def stream_sidecar(*_args, **_kwargs):
    return '{"test": "test_sidecar"}'.encode("utf-8"), 200


def test_get_sidecar(core):
    acq_files = {
        "files": [
            {"name": "my_file.json", "type": "dicom", "file_id": "1"},
            {"name": "my_file.json", "type": "source code", "file_id": "2"},
            {"name": "my_test_file.json", "type": "source code", "file_id": "4"},
            {
                "name": "my_test_file_imaginary.json",
                "type": "source code",
                "file_id": "5",
            },
        ]
    }
    core.add_response("/api/acquisitions/1", acq_files)
    core.add_callback("/api/files/5/download", stream_sidecar)
    file_ = MagicMock()
    file_.location.name = "my_test_file_imaginary.nii.gz"
    file_.hierarchy = {"type": "acquisition", "id": 1}
    fw = CoreClient(url=core.url, api_key="test")
    file_contents, file_dict = get_sidecar(fw, file_.location.name, file_.hierarchy)
    assert file_dict["name"] == "my_test_file_imaginary.json"
    assert file_contents == {"test": "test_sidecar"}


def test_get_sidecar_not_exist(core, monkeypatch):
    monkeypatch.setenv("NIFTI_RETRY_TIME", "1")
    acq_files = {
        "files": [
            {"name": "my_file.json", "type": "dicom", "file_id": "1"},
        ]
    }
    core.add_response("/api/acquisitions/1", acq_files)
    core.add_callback("/api/files/4/download", stream_sidecar)
    file_ = MagicMock()
    file_.location.name = "my_test_file_imaginary.nii.gz"
    file_.hierarchy = {"type": "acquisition", "id": 1}
    fw = CoreClient(url=core.url, api_key="test")
    with pytest.raises(ValueError):
        _ = get_sidecar(fw, file_.location.name, file_.hierarchy)


def test_nifti_fw_adapter_get_sidecar_cant_download(core, monkeypatch):
    monkeypatch.setenv("NIFTI_RETRY_TIME", "1")
    acq_files = {
        "files": [
            {"name": "my_file.json", "type": "source code", "file_id": "1"},
        ]
    }
    core.add_response("/api/acquisitions/1", acq_files)

    def _sidecar(*_args, **_kwargs):
        return b"", 401

    core.add_callback("/api/files/1/download", _sidecar)
    file_ = MagicMock()
    file_.location.name = "my_file.nii.gz"
    file_.hierarchy = {"type": "acquisition", "id": 1}
    fw = CoreClient(url=core.url, api_key="test")
    with pytest.raises(ClientError):
        _ = get_sidecar(fw, file_.location.name, file_.hierarchy)


def test_get_metadata_from_hierarchy(core, mocker):
    fw = CoreClient(url=core.url, api_key="test")
    core.add_response(
        "/api/acquisitions/1",
        {"parents": {"subject": 1, "session": 1}, "uid": "my_uid"},
    )
    core.add_response(
        "/api/sessions/1", {"label": "test_StudyID", "uid": "session_uid"}
    )
    core.add_response(
        "/api/subjects/1",
        {"firstname": "test1", "lastname": "test2", "label": "test_label"},
    )
    sidecar = mocker.patch("fw_gear_nifti_to_dicom.metadata.get_sidecar")
    sidecar.return_value = ({"SOPInstanceUID": "test_sop"}, {"modality": "MR"})
    out = get_metadata(fw, "test", {"type": "acquisition", "id": 1})

    assert out["PatientName"] == "test2^test1"
    assert out["PatientID"] == "test_label"
    assert out["StudyID"] == "test_StudyID"
    assert out["SeriesNumber"] == 0
    assert out["InstanceNumber"] == 0
    assert out["Modality"] == "MR"
    assert out["SOPClassUID"] == "1.2.840.10008.5.1.4.1.1.4.1"


def test_get_metadata_from_sidecar(core, mocker):
    fw = CoreClient(url=core.url, api_key="test")
    core.add_response(
        "/api/acquisitions/1", {"parents": {"subject": 1, "session": 1}, "uid": None}
    )
    core.add_response("/api/sessions/1", {"uid": None})
    core.add_response("/api/subjects/1", {"uid": None})
    gen_uid = mocker.patch("fw_gear_nifti_to_dicom.metadata.generate_uid")
    sidecar = mocker.patch("fw_gear_nifti_to_dicom.metadata.get_sidecar")
    sidecar.return_value = (
        {
            "PatientName": "testname",
            "PatientID": "testid",
            "StudyID": "testname",
            "SeriesNumber": 1,
            "InstanceNumber": 1,
            "Modality": "CT",
        },
        {},
    )
    out = get_metadata(fw, "test", {"type": "acquisition", "id": 1})

    assert out["PatientName"] == "testname"
    assert out["PatientID"] == "testid"
    assert out["StudyID"] == "testname"
    assert out["SeriesNumber"] == 1
    assert out["InstanceNumber"] == 1
    assert out["Modality"] == "CT"
    assert out["SOPClassUID"] == "1.2.840.10008.5.1.4.1.1.2.1"
    assert out["SeriesInstanceUID"] == gen_uid.return_value
    assert out["StudyInstanceUID"] == gen_uid.return_value
    assert out["SOPInstanceUID"] == gen_uid.return_value
