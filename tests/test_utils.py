import gzip

import pytest

from fw_gear_nifti_to_dicom.utils import is_gzip


@pytest.mark.parametrize("closed", (True, False))
@pytest.mark.parametrize("side_effect, out", [(None, True), (gzip.BadGzipFile, False)])
def test_is_gzip(mocker, side_effect, out, closed):
    gzip = mocker.patch("fw_gear_nifti_to_dicom.utils.gzip.open")
    gzip.side_effect = side_effect
    gzip.return_value.closed = closed

    assert is_gzip("test") == out
    if not closed and out:
        gzip.return_value.close.assert_called_once()
