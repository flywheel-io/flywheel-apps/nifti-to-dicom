import subprocess
from contextlib import nullcontext as does_not_raise

import pytest

from fw_gear_nifti_to_dicom.pixelmed import run_pixelmed_conversion


@pytest.mark.parametrize(
    "exitcode,raises", [(0, does_not_raise()), (1, pytest.raises(SystemExit))]
)
def test_pixelmed(mocker, tmp_path, monkeypatch, exitcode, raises):
    monkeypatch.setenv("PIXELMED", "test")
    sp = mocker.patch("fw_gear_nifti_to_dicom.pixelmed.subprocess.Popen")
    sp.return_value.poll.side_effect = (None, None, 1)
    sp.return_value.returncode = exitcode
    sp.return_value.communicate.side_effect = ((b"test", ""), (b"test2", ""))
    (tmp_path / "test_in").touch()
    metadata = {
        "PatientName": "test1",
        "PatientID": "test2",
        "StudyID": "test3",
        "SeriesNumber": "test4",
        "InstanceNumber": "test5",
        "Modality": "test6",
        "SOPClassUID": "test7",
    }
    with raises:
        res = run_pixelmed_conversion(
            tmp_path / "test_in", "test_out", tmp_path, metadata
        )
        assert res == tmp_path / "test_out"
        sp.assert_called_once_with(
            [
                "java",
                "-cp",
                "test",
                "com.pixelmed.convert.NIfTI1ToDicom",
                str(tmp_path / "test_in"),
                str(tmp_path / "test_out"),
                "test1",
                "test2",
                "test3",
                "test4",
                "test5",
                "test6",
                "test7",
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
