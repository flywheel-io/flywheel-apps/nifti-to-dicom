import pytest

pytest_plugins = ("fw_http_testserver",)


@pytest.fixture
def core(http_testserver):
    return http_testserver
