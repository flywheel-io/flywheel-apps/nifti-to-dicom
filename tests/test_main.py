"""Module to test main.py"""
from pathlib import Path
from unittest.mock import MagicMock

import pytest

from fw_gear_nifti_to_dicom.main import run


def test_run(mocker, tmp_path):
    get_meta = mocker.patch("fw_gear_nifti_to_dicom.main.get_metadata")
    run_pixel = mocker.patch("fw_gear_nifti_to_dicom.main.run_pixelmed_conversion")
    (tmp_path / "test.nii.gz").touch()
    in_dict = {
        "location": {
            "path": str(tmp_path / "test.nii.gz"),
        },
        "hierarchy": {"type": "acquisition", "id": 1},
    }
    fw = MagicMock()
    run(in_dict, fw, tmp_path)
    get_meta.assert_called_once_with(
        fw, Path(tmp_path / "test.nii.gz"), in_dict["hierarchy"]
    )
    assert run_pixel.call_args[0][1] == "test.dcm"
    assert run_pixel.call_args[0][2] == Path(tmp_path)
    assert run_pixel.call_args[0][3] == get_meta.return_value
